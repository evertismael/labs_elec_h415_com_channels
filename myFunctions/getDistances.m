function d = getDistances(initCoord,coordinates)
    % Input:
    % initCoord: Coordinates of initial point (COORDINATES OF ANTENNA)
    % coordinates: Matrix with the coordinates. - provided by measurements
    
    R = 6.371e6;    % Earth radious
    phi_1 = initCoord(1);
    lambda_1 = initCoord(2);
    [N,~] = size(coordinates);
    d = zeros(N,1);
    for i=1:1:N
        phi_2 = coordinates(i,1);
        lambda_2 = coordinates(i,2);
        
        diff_phi = (phi_2-phi_1)/2;
        diff_lambda = (lambda_2-lambda_1)/2;
        
        d(i) = 2*R*asin(sqrt((sind(diff_phi))^2 ...
                      + cosd(phi_1)*cosd(phi_2)*((sind(diff_lambda))^2)));
                  
    end
end