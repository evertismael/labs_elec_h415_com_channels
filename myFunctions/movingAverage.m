function outData = movingAverage(inData,n)
    % INPUTS:
    % inData    : Data to be filtered.
    % n         : size of the window. (MUST BE ODD)  
    window = (1/n)*ones(n,1);
    K = length(inData);
    % padding inData.
    m = (n-1)/2;
    inData = [ones(m,1)*inData(1);inData;ones(m,1)*inData(end)];
    outData = zeros(K,1);
    for i=m+1:1:K
        outData(i) = sum(window.*inData(i:i+n));
    end    
end