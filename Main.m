%--------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('Friday/Slot 3'));
addpath(genpath('myFunctions'));
load('data.mat');
%-----------------------------------------------------------
% --------- Antenna parametters ----------------
antenna_lat = 50.812116;
antenna_long = 4.384953;
antenna_frec = 925600000; % Hz
antenna_waveLength = 299792458 / antenna_frec;
antennaCoordinates = [antenna_lat,antenna_long];
% =====================================================================
% ======================== 4.1: POST-PROCESSING =======================
% =====================================================================

% --------- Plot the received power on a Google Map ----------
%figure
%power_map(power, coordinates);

% - Plot the received power as a function of distance to the base station
d = getDistances(antennaCoordinates,coordinates);

uCoordinates = coordinates(5000:end,:);
uPower = power(:,5000:end);
uD = d(5000:end,:);
utime = time(:,5000:end);
%figure
%plot(uD',uPower);
%title('experimental Data');
%grid 

% =====================================================================
% ======================== 4.2: PATH LOSS =============================
% =====================================================================

% ----------------- Extract the Path Loss ----------------
% ----------------- Extract the Path Loss model ----------------
logD = log10(uD');
p = polyfit(logD,uPower,1);
pathLoss = polyval(p,logD);
pathLossExponent = (-p(1)/10);

% ---------- Deduce the maximal cell radious (without shadowing) -------
max_Attenuation = -102;
max_lD = (max_Attenuation - p(2))/p(1);
max_radious = 10^max_lD;

% -- plotting
%figure
%plot(logD,pathLoss);
%hold on;
%plot(logD,uPower);
%grid;

disp('================= 4.2: PATH LOSS');
disp(['Max Cell radious: ', num2str(max_radious), ' [m]']);
disp(['PatLoss exponent: ', num2str(pathLossExponent)]);

% =====================================================================
% ======================== 4.3: SHADOWING =============================
% =====================================================================
% actual Power = 
%       shadowing(lambda scale) (blockage and diffraction by obstacles) 
%     + fastFading(tens scale) (interf. various waves arriving on the antenna)



% ----------------- Extract the Shadowing ----------------
d_in2samples = sum(uD(2:end)-uD(1:end-1))/length(uD(1:end-1)); % average of distance between samples.
windowSize = ceil((2)*antenna_waveLength/d_in2samples);
shadowing =  movmean(uPower,windowSize) - pathLoss;
% Give a statistical shadowing model % USE THE TOOL: 'dfittool'
disp('================= 4.3: SHADOWING ');
pd_sh = fitdist(shadowing','Normal');
disp(['Variance: ', num2str(pd_sh.sigma^2)]);

% Evaluate the shadowing correlation distance r_c (for ro < 0.37 );
[acf,lags,bounds] = autocorr(shadowing,3000);
rc = lags(find(acf<0.37,1))*d_in2samples;
disp(['Correlation Distance rc: ', num2str(rc),'[m]']);

% What is the maximal cell radius if 90% connection prob at the cell edge
% is chosen?

% equation 3.54 % graphically
gamma = -20:0.001:20;
pr = 0.5*erfc(gamma./(pd_sh.sigma*sqrt(2)));
%plot(gamma,pr);
gamma_90 = gamma(find(pr<=(1-0.90),1));
avgPathLoss90 = max_Attenuation + gamma_90;
max_LogDist90 = (avgPathLoss90 - p(2))/p(1);
max_dist90 = 10^max_LogDist90;
disp(['maximal cell radius 90% edge.: ', num2str(max_dist90),'[m]']);

% What is the maximal cell radius if 95% connection probability is required
% through the whole cell?

b = (10*pathLossExponent*log10(exp(1)))/(sqrt(2)*pd_sh.sigma);
a = 0:0.001:2;
fu = 1 - 0.5*erfc(a) + 0.5*(exp(2*a/b + 1/(b^2))).*erfc(a + 1/b);

% plot(a,fu);
% grid;

a_95 = a(find(fu>(0.95),1));
gamma95 = a_95*pd_sh.sigma*sqrt(2);
avgPathLoss95 = max_Attenuation + gamma95;
max_LogDist95 = (avgPathLoss95 - p(2))/p(1);
max_dist95 = 10^max_LogDist95;
disp(['maximal cell radius 95% whole cell.: ', num2str(max_dist95),'[m]']);

% =====================================================================
% ======================== 4.4: CORRELATD SHADOWING MODEL =============
% =====================================================================


T_in2samples = sum(utime(2:end)-utime(1:end-1))/length(utime(1:end-1)); % average of distance between samples.

v = d_in2samples /  T_in2samples;
utime = utime - utime(1);
a = exp(-(v*T_in2samples)/rc);
for k =1:length(utime)
    if k==1
        u(k) = randn;
    else
        u(k) = randn + u(k-1)*a;
    end
   logY(k) = u(k) * pd_sh.sigma*sqrt(1-a^2);
end
plot(logY)




% -- plotting
%figure
%plot(logD,uPower);
%hold on;
%plot(logD,pathLoss+shadowing,'LineWidth',1);
%grid;
